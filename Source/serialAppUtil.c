

#include "bcomdef.h"
#include "OSAL.h"
#include "OSAL_PwrMgr.h"

#include "OnBoard.h"
#include "hal_adc.h"
#include "hal_led.h"
#include "hal_key.h"
#include "hal_lcd.h"

#include "gatt.h"

#include "gapgattserver.h"
#include "gattservapp.h"
#include "devinfoservice.h"
#include "simpleGATTprofile.h"

#if defined( CC2540_MINIDK )
  #include "simplekeys.h"
#endif

#if defined ( PLUS_BROADCASTER )
  #include "peripheralBroadcaster.h"
#else
  #include "peripheral.h"
#endif

#include "gapbondmgr.h"

#include "simpleBLEPeripheral.h"

#include "serialAppUtil.h"
#include "hal_uart.h"


#define reportQEmpty()                        ( firstQIdx == lastQIdx )

/*********************************************************************
 * CONSTANTS
 */
#define MAX_SAPP_PACKET_LEN        20
#define MAX_SAPP_NUM_ENTRIES       30


/*********************************************************************
 * TYPEDEFS
 */


/*********************************************************************
 * LOCAL VARIABLES
 */
static uint8 sendMsgTo_TaskID;

// Pending reports
static uint8 firstQIdx = 0;
static uint8 lastQIdx = 0;
static attHandleValueNoti_t packetQ[MAX_SAPP_NUM_ENTRIES];

static uint8 pparsebuff = 0;
/*********************************************************************
 * LOCAL FUNCTIONS
 */

static void sappEnqueue( attHandleValueNoti_t* );
static attHandleValueNoti_t *packetDequeue( void );


/*********************************************************************
 * PUBLIC FUNCTIONS
 */

void SerialApp_Init( uint8 taskID )
{

  serialAppInitTransport();

  // initialize the task for SAPP-Controller
  sendMsgTo_TaskID = taskID;
}

void serialAppInitTransport( )
{
  halUARTCfg_t uartConfig;

  // configure UART
  uartConfig.configured           = TRUE;
  uartConfig.baudRate             = SBP_UART_BR;
  uartConfig.flowControl          = SBP_UART_FC;
  uartConfig.flowControlThreshold = SBP_UART_FC_THRESHOLD;
  uartConfig.rx.maxBufSize        = SBP_UART_RX_BUF_SIZE;
  uartConfig.tx.maxBufSize        = SBP_UART_TX_BUF_SIZE;
  uartConfig.idleTimeout          = SBP_UART_IDLE_TIMEOUT;
  uartConfig.intEnable            = SBP_UART_INT_ENABLE;
  uartConfig.callBackFunc         = sbpSerialPacketParser;

  // start UART
  // Note: Assumes no issue opening UART port.
  (void)HalUARTOpen( SBP_UART_PORT, &uartConfig );

  return;
}


/*******************************************************************************
 * @fn          sappSerialPacketParser
 *
 * @brief       This routine is the UART callback function. It builds a BLE
 *              command or data packet (depending on the packet type) from bytes
 *              recieved by the SAPP via the serial port.
 *
 *              SAPP command packet format:
 *              Packet Type + Command opcode + lengh + command payload
 *              | 1 octet   |      2         |   1   |      n        |
 *
 *              SAPP data packet format:
 *              Packet Type +   Conn Handle  + lengh +  data payload
 *              | 1 octet   |      2         |   2   |      n      |
 *
 * input parameters
 *
 * @param       port  - UART callback serial port.
 * @param       event - UART callback event number.
 *
 * output parameters
 *
 * @param       None.
 *
 * @return      None.
 */
void sbpSerialPacketParser( uint8 port,
                            uint8 event )
{
  static uint8  sappPktState = SAPP_PARSER_STATE_PKT_TYPE;
  static uint8  pktType;
  static uint16 param1;   // opcode for command, connection handle for data
  static uint16 pktLen;
  

  
  //
  uint16 numBytes;

  // unused input parameter; PC-Lint error 715.
  (void)event;

  // check if there's any serial port data to process
  if ( (numBytes = Hal_UART_RxBufLen(port)) > 0 )
  {
    // yes, so check if we are at the start of a new packet
    if ( sappPktState == SAPP_PARSER_STATE_PKT_TYPE )
    {
      // yes, so read the packet type
      // Note: Assumes we'll get the data indicated by Hal_UART_RxBufLen.
      (void)HalUARTRead (port, &pktType, 1);

      numBytes -= 1;

      // set next state based on the type of packet
      // Note: Events are never sent to the target.
      switch( pktType )
      {
        case SAPP_CMD_PACKET:
          sappPktState = SAPP_CMD_PARSER_STATE_OPCODE;
          break;

        case SAPP_ACL_DATA_PACKET:
        case SAPP_SCO_DATA_PACKET:
          sappPktState = SAPP_DATA_PARSER_STATE_HANDLE;
          break;

        default:
          // illegal packet type
          return;

        // Note: Unreachable statement generates compiler warning!
        //break;
      }
    }

    // process serial port bytes to build the command or data packet
    switch( sappPktState )
    {
      // command opcode
      case SAPP_CMD_PARSER_STATE_OPCODE:
        if (numBytes < 2) break;

        // read the opcode
        // Note: Assumes we'll get the data indicated by Hal_UART_RxBufLen.
        (void)HalUARTRead (port, (uint8 *)&param1, 2);

        numBytes -= 2;

        sappPktState = SAPP_CMD_PARSER_STATE_LENGTH;

        // DROP THROUGH
        //lint -fallthrough

      // command length
      case SAPP_CMD_PARSER_STATE_LENGTH:
        if (numBytes < 1) break;

        // clear length before setting since only one byte of length is valid
        pktLen = 0;

        // read the length
        // Note: Assumes we'll get the data indicated by Hal_UART_RxBufLen.
        (void)HalUARTRead (port, (uint8 *)&pktLen, 1);

        numBytes -= 1;

        sappPktState = SAPP_CMD_PARSER_STATE_DATA;

        // DROP THROUGH
        //lint -fallthrough

      // command payload
      case SAPP_CMD_PARSER_STATE_DATA:
        // check if there is enough serial port data to finish command packet
        if ( numBytes >= pktLen )
        {
          sappPacket_t *pMsg;

          // there's enough serial data to finish this packet, so allocate memory
          pMsg = (sappPacket_t *)osal_msg_allocate( sizeof (sappPacket_t) +
                                                   SAPP_CMD_MIN_LENGTH   +
                                                   pktLen );

          // if we have a block of allocated memory, then fill it
          if ( pMsg )
          {
            // fill in message data
            pMsg->pData    = (uint8*)(pMsg+1);
            pMsg->pData[0] = pktType;
            pMsg->pData[1] = ((uint8 *)&param1)[0]; // opcode (LSB)
            pMsg->pData[2] = ((uint8 *)&param1)[1]; // opcode (MSB)
            pMsg->pData[3] = ((uint8 *)&pktLen)[0]; // one byte of length for cmd

            // copy serial data into message
            // Note: Assumes we'll get the data indicated by Hal_UART_RxBufLen.
            (void)HalUARTRead (port, &pMsg->pData[4], pktLen);

            // set header specific fields
            pMsg->hdr.status = 0xFF;

            // check if a Controller Link Layer VS command
            if (  ((param1 >> 10) == VENDOR_SPECIFIC_OGF) &&
                 (((param1 >> 7) & 0x07) != SAPP_OPCODE_CSG_LINK_LAYER) )
            {
              // this is a vendor specific command
              pMsg->hdr.event = SAPP_EXT_CMD_EVENT;

              // so strip the OGF (i.e. the most significant 6 bits of the opcode)
              pMsg->pData[2] &= 0x03;

              // and send it to the vendor specific extension handler
              // Note: The HostTest project has an SAPP Extension task with task ID
              //       SAPP_ExtApp_TaskID, which is used in the call to SAPP_ExtTaskRegister
              //       which assigns it to sappExtTaskID. The event SAPP_EXT_CMD_EVENT only
              //       exists in the HostTest project where it is handled by
              //       SAPP_EXT_App_ProcessEvent. Since sappExtTaskID is the same
              //       as SAPP_ExtApp_TaskID, this event is handled directly by
              //       SAPP_EXT_App_ProcessEvent in the HostTest project.
             
              
              (void)osal_msg_send( sendMsgTo_TaskID, (uint8 *)pMsg );
              
              //counter2++;
              //LCD_WRITE_STRING_VALUE( "D2 ", counter2, 10,3 ); 
              
            }
            else // specification specific command
            {
              // this is a normal host-to-controller event
              pMsg->hdr.event = SAPP_HOST_TO_CTRL_CMD_EVENT;

              // so send it to the SAPP handler
              (void)osal_msg_send( sendMsgTo_TaskID, (uint8 *)pMsg );
            }

            // ready to start the next packet
            sappPktState = SAPP_PARSER_STATE_PKT_TYPE;
          }
        }
        
        break;

      case SAPP_DATA_PARSER_STATE_HANDLE:
        if (numBytes < 2) break;

        // read the data connection handle
        // Note: Assumes we'll get the data indicated by Hal_UART_RxBufLen.
        (void)HalUARTRead (port, (uint8 *)&param1, 2);

        numBytes -= 2;

        sappPktState = SAPP_DATA_PARSER_STATE_LENGTH;

        // DROP THROUGH
        //lint -fallthrough

      case SAPP_DATA_PARSER_STATE_LENGTH:
        if (numBytes < 2) break;

        // read the length
        // Note: Assumes we'll get the data indicated by Hal_UART_RxBufLen.
        (void)HalUARTRead (port, (uint8 *)&pktLen, 2);

        // verify the packet length is valid for our max input buffer size
        if ( pktLen >= SAPP_UART_RX_BUF_SIZE )
        {
          // something is wrong, so start over again
          sappPktState = SAPP_PARSER_STATE_PKT_TYPE;
          break;
        }

        numBytes -= 2;

        sappPktState = SAPP_DATA_PARSER_STATE_DATA;

        // DROP THROUGH
        //lint -fallthrough

      case SAPP_DATA_PARSER_STATE_DATA:
        // check if there is enough serial port data to finish data packet
        if ( numBytes >= pktLen )
        {
          sappDataPacket_t *pMsg;

          // there's enough serial data to finish this packet; allocate memory
          pMsg = (sappDataPacket_t *)osal_msg_allocate( sizeof(sappDataPacket_t) );

          // if we have a block of allocated memory, then fill it
          if ( pMsg )
          {
            pMsg->hdr.event  = SAPP_HOST_TO_CTRL_DATA_EVENT;
            pMsg->hdr.status = 0xFF;

            // fill in message data
            pMsg->pktType    = pktType;
            pMsg->connHandle = param1 & 0x0FFF;         // mask out PB and BC flags
            pMsg->pbFlag     = (param1 & 0x3000) >> 12; // isolate PB flag
            pMsg->pktLen     = pktLen;
            pMsg->pData      =  osal_mem_alloc(pktLen);

            // check if we have a BM buffer for payload
            if ( pMsg->pData )
            {
              // copy serial data into message
              // Note: Assumes we'll get the data indicated by Hal_UART_.
              (void)HalUARTRead (port, pMsg->pData, pktLen);

              // send the message
              (void)osal_msg_send( sendMsgTo_TaskID, (uint8 *)pMsg );

              // ready to start the next packet
              sappPktState = SAPP_PARSER_STATE_PKT_TYPE;
            }
            else // no memory available for payload
            {
              // so give back memory allocated for message
              (void)osal_msg_deallocate( (uint8 *)pMsg );
            }
          }
        }

        break;

      default:
        //ASSERT;
        break;
    } // switch
  } // if
  return;
}

/********************************************
 * 
 * Send GATT Notification
 * - Called from timer
 * - Try send 4 packets
 * - On fail set flag to not deQueue until sent successfully
 */
void sappSendNotification()
{
  //if(startsnd == 1)
 // {
    uint8 i = 0; 
    static attHandleValueNoti_t *pReport= NULL;
    
     
     
    if ( pReport == NULL)
    {
       pReport = packetDequeue();
       
       if ( pReport == NULL )
       {
         return;
       }
    }
    
    //try 3 packets per connection event
    for ( i = 0; i<3 ; i++)
    {
      if ( GATT_Notification( 0, pReport, FALSE )==SUCCESS)
      {
        
        //counterOut++;
        //LCD_WRITE_STRING_VALUE( "DOUT ", ++counterOut, 10,2 ); 
        
        //get next
     // if((((RFERRF&0x04)>>2))!=1)
      //{
        //gattstat = GATT_Notification( 0, pReport, FALSE );
        
        //if(gattstat !=0)
        //{
         // if(gattstat == 0x04)
         // {
          //  datafail++;
         // }
          //gattstat2 = gattstat;
         // datafail = 1;
         // i=3;
          
        //  
       // }
          
          
        pReport = packetDequeue();
        if ( pReport == NULL )
        {
          i=3;
        }
      
      //}  
      }
    
    
    
  
  //}
      else
      {
        pReport = NULL;
        i=3;
      }
    }
}

/*************************************************
 *
 * Recieve data from serial port
 * - Queue data
 * - Start timer to send GATT Notifications
 */
void sappSendData( attHandleValueNoti_t *pData )
{
 

    sappEnqueue( pData );
 
}  

/*********************************************************************
 * @fn      sappEnqueue
 *
 * @brief   Enqueue a HID report to be sent later.
 *
 * @param   id - HID report ID.
 * @param   type - HID report type.
 * @param   len - Length of report.
 * @param   pData - Report data.
 *
 * @return  None.
 */
static void sappEnqueue( attHandleValueNoti_t *pData )
{
  
  // Update last index
  lastQIdx = ( lastQIdx + 1 ) % MAX_SAPP_NUM_ENTRIES;

  if ( lastQIdx == firstQIdx )
  {
    // Queue overflow; discard oldest report
    firstQIdx = ( firstQIdx + 1 ) % MAX_SAPP_NUM_ENTRIES;
  }

  // stored as notification
  packetQ[lastQIdx].len =pData->len;
  osal_memcpy( &packetQ[lastQIdx], pData, pData->len+3 );
  
  
   //counterIn++;
  // LCD_WRITE_STRING_VALUE( "DIN  ", ++counterIn, 10,3 ); 
  
 
}

/*********************************************************************
 * @fn      packetDequeue
 *
 * @brief   Dequeue a HID report to be sent out.
 *
 * @param   id - HID report ID.
 * @param   type - HID report type.
 * @param   len - Length of report.
 * @param   pData - Report data.
 *
 * @return  None.
 */
static attHandleValueNoti_t *packetDequeue( void )
{
  if ( reportQEmpty() )
  {
    return NULL;
  }

  // Update first index
  firstQIdx = ( firstQIdx + 1 ) % MAX_SAPP_NUM_ENTRIES;

  return ( &(packetQ[firstQIdx]) );
}


/******************************************
*/

void dmasend(dmadata_t * ddata)
{
  
   static attHandleValueNoti_t pdmasend;
   static uint8 dlength;
   static uint8 ddbuff[85];
  
   pdmasend.len = 20;
   pdmasend.handle = 20;
   osal_memcpy( ddbuff, ddata->dmadata, 85);
   static uint8 pdmadata;
   static int q;
 
   dlength = ((((ddata->channellength)*8)*4)+3);
 

   while(q<dlength)
   {
    pdmadata = q++;

    pdmasend.value[pparsebuff++] = ddbuff[pdmadata];
    if(pparsebuff>19)
    {
      pparsebuff = 0;
      
      sappSendData(&pdmasend);
      
    }
    
     
   }
  
  q = 0;
  
}



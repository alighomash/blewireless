/**************************************************************************************************
  Filename:       cma3000d.c
  Revised:        $Date: 2013-02-08 05:16:52 -0800 (Fri, 08 Feb 2013) $
  Revision:       $Revision: 33023 $

  Description:    Control of the accelerometer on the keyfob board in the CC2540DK-mini
                  kit.

  Copyright 2009 - 2010 Texas Instruments Incorporated. All rights reserved.

  IMPORTANT: Your use of this Software is limited to those specific rights
  granted under the terms of a software license agreement between the user
  who downloaded the software, his/her employer (which must be your employer)
  and Texas Instruments Incorporated (the "License").  You may not use this
  Software unless you agree to abide by the terms of the License. The License
  limits your use, and you acknowledge, that the Software may not be modified,
  copied or distributed unless embedded on a Texas Instruments microcontroller
  or used solely and exclusively in conjunction with a Texas Instruments radio
  frequency transceiver, which is integrated into your product.  Other than for
  the foregoing purpose, you may not use, reproduce, copy, prepare derivative
  works of, modify, distribute, perform, display or sell this Software and/or
  its documentation for any purpose.

  YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
  PROVIDED �AS IS� WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
  INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
  NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
  TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
  NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
  LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
  INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
  OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
  OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
  (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

  Should you have any questions regarding your right to use this Software,
  contact Texas Instruments Incorporated at www.TI.com.
**************************************************************************************************/

#include <ioCC2540.h>
#include "cma3000d.h"
#include "simpleBLEPeripheral.h"


//***********************************************************************************
// Defines

// Accelerometer connected at (rev0.6/rev1.0):
// P0_6 = DATA_READY/-
// P1_2 = -/CS
// P1_3 = SCK
// P1_4 = MISO
// P1_5 = MOSI
// P1_7 = CS/DATA_READY

#define SCK             P1_3
#define MISO            P1_4
#define MOSI            P1_5

#ifdef REV_1_0
  #define CS              P1_2
#elif (defined REV_0_6)
  #define CS              P1_7
#endif

#define CS_DISABLED     1
#define CS_ENABLED      0

#define SDATAC         0x11
#define RDATAC         0x10
#define STOP           0x0A


#define CONFIG1        0x01
#define CONFIG2        0x02
#define CONFIG3        0x03
#define CH1SET         0x05 
#define CH2SET         0x06
#define CH3SET         0x07
#define CH4SET         0x08
#define CH5SET         0x09
#define CH6SET         0x0A
#define CH7SET         0x0B
#define CH8SET         0x0C

#define RLD_SENSP      0x0D

//***********************************************************************************
// Function prototypes



//***********************************************************************************
// Local variables
static uint8 acc_initialized = FALSE;

static uint8 altest[27];

static uint8 spidummy = 0x00;



static uint8 regread = 0x00;

static uint8 * pregread = &regread;

;






/** \brief	Initialize SPI interface and CMA3000-D01 accelerometer
*
* This will initialize the SPI interface and CMA3000-D01 accelerometer
*
*/
void accInit(void)
{
    //*** Setup USART 0 SPI at alternate location 2 ***

    // USART 0 at alternate location 2
    PERCFG |= 0x00;
    // Peripheral function on SCK, MISO and MOSI (P1_3-5)
    P0SEL |= 0x2C;
    // Configure(P0_4/P0_0/P0_1) as output
    P0SEL &= ~0x93;
    P0DIR |= 0x13;
    P0DIR &= ~0x80; //DRDY as input
      
    
    
    
    //Configure(P1_7/P1_6) as output
    
     P1SEL &= ~0xE0;//c0
     P1DIR |= 0xE0;//c5
     
    //p2.0 start 
    
     P2SEL &= ~0x01;
     P2DIR |= 0x01;
    
     
    
    //reset high/PWDN low
     P1 |= 0x80;
     P1 &= ~0x40;
     P1 |= 0x20;//for eval board start pin
    //p0.1 = high/p0.0 = low/p0.4 = low
     
    P0 |= 0x02; 
    P0 &= ~0x11;
   
    //p2.0 low
    
    P2 |= 0x01;
  
    
    //Set P0 to detect falling edge for interrupt
    
    PICTL |= 0x01;
    
    //P0_7 Interrupt enable
    
    //P0IEN = 0x80;
    
    //IEN1 &= 0x20;
    
    IEN1 &= ~0x20;
    
    IEN0 &= ~0x80;
    
     //test
    //**************************
   // P0IEN = 0x80;
   // IEN1 &= 0x20;
    
    
    
//****************    

    //CS = CS_DISABLED;   

    //*** Setup the SPI interface ***
    // SPI master mode
    U0CSR = 0x00;
    // Negative clock polarity, Phase: data out on CPOL -> CPOL-inv
    //                                 data in on CPOL-inv -> CPOL
    // MSB first
    U0GCR = 0x20;
    // SCK frequency = 480.5kHz (max 500kHz)
    U0GCR |= 0x0F;//0x0F//0x11//0C//0x12
    U0BAUD = 0xD8;//0xD8//0x00//D8//0x00

    
    U0GCR &= ~0xC0;  

    U0GCR |= 0x40;
    U0GCR |= 0x20;  // ORDER = 1 
    
    
    
    WAIT_1_3US(80);
 
    
    P1 |= 0x40;
    
    
    
    //Reset pulse 
    
    //P1 &= ~0x80;
    P1 |= 0x80;
    P1 |= 0x40;
    
    WAIT_1_3US(80);
     
    for(int i = 0; i<4000;i++)
        WAIT_1_3US(250);//wait for 1s
    P1 &= ~0x80;
 
     
    WAIT_1_3US(250);
    P1 |= 0x80;
    
    
    //*************for eval board only:
    P1SEL &= ~0x10;
    P1DIR &= ~0x10;
    
    P1IEN |= 0x10;
    PICTL |=0x06;
    
    
    
    //*************
    
    for(int i = 0; i<4000;i++)
        WAIT_1_3US(250);//wait for 1s
    
    
    
    
    
  
    spiWriteByte(SDATAC);
    
 
  
    spiWriteByte(SDATAC);
    
    WAIT_1_3US(80);
    
    spiWriteByte(STOP);
    WAIT_1_3US(80);
   
    //accWriteReg(CONFIG3, 0xC0);
    accWriteReg(CONFIG3, 0xCC);
    
    
    accWriteReg(CONFIG1,  0x06);
    
    accWriteReg(CONFIG2,  0x00);
    
    accWriteReg(CH1SET,  0x01);
    accWriteReg(CH2SET,  0x01);
    accWriteReg(CH3SET,  0x01);
    accWriteReg(CH4SET,  0x01);
    accWriteReg(CH5SET,  0x01);
    accWriteReg(CH6SET,  0x01);
    accWriteReg(CH7SET,  0x01);
    accWriteReg(CH8SET,  0x01);
    
    
    accReadReg(CONFIG1,pregread);
    
    WAIT_1_3US(80);
    
    
  
    
    
    //Clear Interrupt Flag
    spiWriteByte(RDATAC);
    
    
    

    while(((P0IFG&0x80)>>7)==0);
 
    P1IFG &= ~0x10;
    
    for(int i = 0; i<27; i++)
    {
      spiReadByte(altest,spidummy);
      
    }
    
    spiWriteByte(SDATAC);
    
    spiWriteByte(STOP);
    WAIT_1_3US(80);
    
    accWriteReg(CONFIG2,  0x12);//fot test signal 0x10//otherwise10
 
    
    
    accWriteReg(CH1SET,  0x00);//0x05 fot test signal//otherwise 0x00
    WAIT_1_3US(80);
    accWriteReg(CH2SET,  0x00);
    WAIT_1_3US(80);
    accWriteReg(CH3SET,  0x00);
    WAIT_1_3US(80);
    accWriteReg(CH4SET,  0x00);
    WAIT_1_3US(80);
    accReadReg(CH1SET,pregread);
    accWriteReg(CH5SET,  0x00);
    WAIT_1_3US(80);
    accWriteReg(CH6SET,  0x00);
    WAIT_1_3US(80);
    accWriteReg(CH7SET,  0x00);
    WAIT_1_3US(80);
    accWriteReg(CH8SET,  0x00);
    
    accReadReg(CH8SET,pregread);
    
    accReadReg(CONFIG1,pregread);
    
    
    WAIT_1_3US(80);
    
    spiWriteByte(RDATAC);
    
    P0 &= ~0x10;
    

  
}

/** \brief	Sets the CMA3000-D01 accelerometer in Power Down mode
*
*/
void accStop(void)
{
  if (acc_initialized) {
     accWriteReg(CTRL, MODE_PD);
     acc_initialized = FALSE;
  }
}

/** \brief	Write one byte to a sensor register
*
* Write one byte to a sensor register
*
* \param[in]       reg
*     Register address
* \param[in]       val
*     Value to write
*/
void accWriteReg(uint8 reg, uint8 val)
{
    //CS = CS_ENABLED;
    spiWriteByte(reg|0x40);
    WAIT_1_3US(80);
    spiWriteByte(0x00);
    WAIT_1_3US(80);
    spiWriteByte(val);
    WAIT_1_3US(80);
    //CS = CS_DISABLED;
}


/** \brief	Read one byte from a sensor register
*
* Read one byte from a sensor register
*
* \param[in]       reg
*     Register address
* \param[in]       *pVal
*     Pointer to variable to put read out value
*/
void accReadReg(uint8 reg, uint8 *pVal)
{
    //CS = CS_ENABLED;
    spiWriteByte(reg|0x20);
    WAIT_1_3US(80);
    spiWriteByte(0x00);
    WAIT_1_3US(80);
    spiReadByte(pVal, 0x00);
    WAIT_1_3US(80);
    //CS = CS_DISABLED;
}




/** \brief	Write one byte to SPI interface
*
* Write one byte to SPI interface
*
* \param[in]       write
*     Value to write
*/
void spiWriteByte(uint8 write)
{
        U0CSR &= ~0x02;                 // Clear TX_BYTE
        U0DBUF = write;
        while (!(U0CSR & 0x02));        // Wait for TX_BYTE to be set
}

/** \brief	Read one byte from SPI interface
*
* Read one byte from SPI interface
*
* \param[in]       read
*     Read out value
* \param[in]       write
*     Value to write
*/
void spiReadByte(uint8 *read, uint8 write)
{
        U0CSR &= ~0x02;                 // Clear TX_BYTE
        U0DBUF = write;
        while (!(U0CSR & 0x02));        // Wait for TX_BYTE to be set
        *read = U0DBUF;
}


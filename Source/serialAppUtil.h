/**************************************************************************************************
  Filename:       serialAppPeripheral.hh
  Revised:        $Date: 2010-08-01 14:03:16 -0700 (Sun, 01 Aug 2010) $
  Revision:       $Revision: 23256 $

  Description:    This file contains the Simple BLE Peripheral sample application
                  definitions and prototypes.

  Copyright 2010 - 2011 Texas Instruments Incorporated. All rights reserved.

  IMPORTANT: Your use of this Software is limited to those specific rights
  granted under the terms of a software license agreement between the user
  who downloaded the software, his/her employer (which must be your employer)
  and Texas Instruments Incorporated (the "License").  You may not use this
  Software unless you agree to abide by the terms of the License. The License
  limits your use, and you acknowledge, that the Software may not be modified,
  copied or distributed unless embedded on a Texas Instruments microcontroller
  or used solely and exclusively in conjunction with a Texas Instruments radio
  frequency transceiver, which is integrated into your product.  Other than for
  the foregoing purpose, you may not use, reproduce, copy, prepare derivative
  works of, modify, distribute, perform, display or sell this Software and/or
  its documentation for any purpose.

  YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
  PROVIDED �AS IS� WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
  INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
  NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
  TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
  NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
  LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
  INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
  OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
  OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
  (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

  Should you have any questions regarding your right to use this Software,
  contact Texas Instruments Incorporated at www.TI.com.
**************************************************************************************************/

#ifndef SERIALAPP_H
#define SERIALAPP_H

#ifdef __cplusplus
extern "C"
{
#endif
  
#include "gatt.h"
   
#define SBP_UART_PORT                  HAL_UART_PORT_0
//#define SBP_UART_FC                    TRUE
  #define SBP_UART_FC                    FALSE
#define SBP_UART_FC_THRESHOLD          48
#define SBP_UART_RX_BUF_SIZE           128
#define SBP_UART_TX_BUF_SIZE           128
#define SBP_UART_IDLE_TIMEOUT          6
#define SBP_UART_INT_ENABLE            TRUE

//#define SBP_UART_BR                   HAL_UART_BR_115200 
#define SBP_UART_BR                     HAL_UART_BR_57600 
  
 
#define SAPP_EXT_LL_SUBGRP                     0x00
#define SAPP_EXT_L2CAP_SUBGRP                  0x01
#define SAPP_EXT_ATT_SUBGRP                    0x02
#define SAPP_EXT_GATT_SUBGRP                   0x03
#define SAPP_EXT_GAP_SUBGRP                    0x04
#define SAPP_EXT_UTIL_SUBGRP                   0x05
#define SAPP_EXT_PROFILE_SUBGRP                0x07  

#define SAPP_EXT_UTIL_RESET                    0x00
#define SAPP_EXT_UTIL_NV_READ                  0x01
#define SAPP_EXT_UTIL_NV_WRITE                 0x02
#define SAPP_EXT_UTIL_FORCE_BOOT               0x03

  
// HCI - Messages IDs (0x90 - 0x9F)
#define SAPP_DATA_EVENT                        0x90 //!< HCI Data Event message
#define SAPP_GAP_EVENT_EVENT                   0x91 //!< GAP Event message
#define SAPP_SMP_EVENT_EVENT                   0x92 //!< SMP Event message
#define SAPP_EXT_CMD_EVENT                     0x93 //!< HCI Extended Command Event message  
  
  
/*
  Minimum length for CMD packet is 1+2+1
  | Packet Type (1) | OPCode(2) | Length(1) |
*/
#define SAPP_CMD_MIN_LENGTH                 4

/* States for Command and Data packet parser */
#define SAPP_PARSER_STATE_PKT_TYPE          0
//
#define SAPP_CMD_PARSER_STATE_OPCODE        1
#define SAPP_CMD_PARSER_STATE_LENGTH        2
#define SAPP_CMD_PARSER_STATE_DATA          3
//
#define SAPP_DATA_PARSER_STATE_HANDLE       4
#define SAPP_DATA_PARSER_STATE_LENGTH       5
#define SAPP_DATA_PARSER_STATE_DATA         6

// SAPP Command Subgroup
#define SAPP_OPCODE_CSG_LINK_LAYER          0
#define SAPP_OPCODE_CSG_CSG_L2CAP           1
#define SAPP_OPCODE_CSG_CSG_ATT             2
#define SAPP_OPCODE_CSG_CSG_GATT            3
#define SAPP_OPCODE_CSG_CSG_GAP             4
#define SAPP_OPCODE_CSG_CSG_SM              5
#define SAPP_OPCODE_CSG_CSG_Reserved        6
#define SAPP_OPCODE_CSG_CSG_USER_PROFILE    7

// Vendor Specific OGF
#define VENDOR_SPECIFIC_OGF                0x3F

// Event Mask Default Values
#define BT_EVT_MASK_BYTE0                  0xFF
#define BT_EVT_MASK_BYTE1                  0xFF
#define BT_EVT_MASK_BYTE2                  0xFF
#define BT_EVT_MASK_BYTE3                  0xFF
#define BT_EVT_MASK_BYTE4                  0xFF
#define BT_EVT_MASK_BYTE5                  0x9F
#define BT_EVT_MASK_BYTE6                  0x00
#define BT_EVT_MASK_BYTE7                  0x20
//
#define LE_EVT_MASK_DEFAULT                0x1F


// SAPP Packet Types
#define SAPP_CMD_PACKET                 0x01
#define SAPP_ACL_DATA_PACKET            0x02
#define SAPP_SCO_DATA_PACKET            0x03
#define SAPP_EVENT_PACKET               0x04  

/* Serial Events */
#define SAPP_CONTROLLER_TO_HOST_EVENT   0x01
#define SAPP_HOST_TO_CTRL_CMD_EVENT     0x02
#define SAPP_HOST_TO_CTRL_DATA_EVENT    0x04

#define SAPP_BDADDR_LEN                 6

/* UART port */
#define SAPP_UART_PORT                  HAL_UART_PORT_0
#define SAPP_UART_FC                    TRUE
#define SAPP_UART_FC_THRESHOLD          48
#define SAPP_UART_RX_BUF_SIZE           128
#define SAPP_UART_TX_BUF_SIZE           128
#define SAPP_UART_IDLE_TIMEOUT          6
#define SAPP_UART_INT_ENABLE            TRUE
  
// LCD macros
#if HAL_LCD == TRUE
#define LCD_WRITE_STRING(str, option)                       HalLcdWriteString( (str), (option))
#define LCD_WRITE_SCREEN(line1, line2)                      HalLcdWriteScreen( (line1), (line2) )
#define LCD_WRITE_STRING_VALUE(title, value, format, line)  HalLcdWriteStringValue( (title), (value), (format), (line) )
#else
#define LCD_WRITE_STRING(str, option)                     
#define LCD_WRITE_SCREEN(line1, line2)                    
#define LCD_WRITE_STRING_VALUE(title, value, format, line)
#endif  


typedef struct
{
  uint8  pktType;
  uint16 opCode;
  uint8  len;
  uint8  *pData;
} sappExtCmd_t;
  
typedef struct
{
  osal_event_hdr_t hdr;
  uint8            *pData;
} sappPacket_t;


typedef struct
{
  osal_event_hdr_t hdr;
  uint8  pktType;
  uint16 connHandle;
  uint8  pbFlag;
  uint16 pktLen;
  uint8  *pData;
} sappDataPacket_t;

typedef struct

{
  uint8 dmaheader;
  uint8 channellength;
  uint8 dmadata[115];
  
} dmadata_t;
  
// Serial Port Related
extern void SerialApp_Init( uint8 taskID );
extern void sbpSerialPacketParser( uint8 port, uint8 event );
extern void serialAppInitTransport(  );
extern void sappSendData( attHandleValueNoti_t *pData  );
extern void sappSendNotification( );
extern uint8 processExtMsg2( sappPacket_t *pMsg );
extern void dmasend(dmadata_t * ddata);
//extern void sappEnqueue( attHandleValueNoti_t* );
  
#ifdef __cplusplus
}
#endif

#endif /* SIMPLEBLEPERIPHERAL_H */
  